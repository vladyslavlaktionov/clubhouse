from IPython.core.display import HTML

from import_data import df
from notebook_setup import image_formatter

df['frequency_invites'] = df['user_id'].map(df['invited_by_user_profile'].value_counts())
df_display = df.sort_values('frequency_invites', ascending=False).head(10)
HTML(df_display.to_html(formatters={'photo_url': image_formatter}, escape=False))