import glob
from PIL import Image, ImageFont, ImageDraw, ImageOps

from import_data import df


def add_corners_and_title(filename, text, color='black'):
    print(filename, text)
    im = Image.open(filename)

    # add rounded corners
    w, h = 400, 400
    rad = w // 2
    im = im.resize((w, h), Image.ANTIALIAS)
    circle = Image.new('L', (rad * 2, rad * 2), 0)
    draw = ImageDraw.Draw(circle)
    draw.ellipse((0, 0, rad * 2, rad * 2), fill=255)
    alpha = Image.new('L', im.size, 255)
    alpha.paste(circle.crop((0, 0, rad, rad)), (0, 0))
    alpha.paste(circle.crop((0, rad, rad, rad * 2)), (0, h - rad))
    alpha.paste(circle.crop((rad, 0, rad * 2, rad)), (w - rad, 0))
    alpha.paste(circle.crop((rad, rad, rad * 2, rad * 2)), (w - rad, h - rad))
    im.putalpha(alpha)

    # add caption
    border = 120
    fontsize = border - 50
    im = ImageOps.expand(im, border=border, fill=(255, 255, 255, 0))
    draw = ImageDraw.Draw(im)
    font = ImageFont.truetype("arial.ttf", fontsize)
    text = text[:20]
    x, y = font.getsize(text)
    draw.text(((w - x + border * 2) // 2, -5), text, color, font=font)

    return im


for filepath in reversed(list(glob.iglob('images/*.jpg'))):
    extracted_id = ''.join([n for n in filepath if n.isdigit()])
    title = df.loc[df['user_id'].isin([int(extracted_id)])].iloc[0]['name']
    im = add_corners_and_title(filepath, title, 'white')
    im.save(filepath.replace('images', 'images_white').replace(".jpg", ".png"))