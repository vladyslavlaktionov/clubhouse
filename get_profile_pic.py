import requests

folder = "images/"


def dl_image(row_id, image_url):
    print(image_url, type(image_url))
    if type(image_url) != str or not "clubhouseprod" in image_url:
        image_url = "https://t3.ftcdn.net/jpg/00/64/67/52/240_F_64675209_7ve2XQANuzuHjMZXP3aIYIpsDKEbF5dD.jpg"
    img_data = requests.get(image_url).content
    with open(folder + str(row_id) + '.jpg', 'wb') as handler:
        handler.write(img_data)


node_pd.apply(lambda row: dl_image(row.Id, row.photo_url), axis=1)