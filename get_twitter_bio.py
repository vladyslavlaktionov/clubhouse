import nest_asyncio
import twint

nest_asyncio.apply()


def get_twitter(username):
    print(username, type(username))
    if username == "null":
        return ["", ""]
    try:
        c = twint.Config()
        c.Username = username
        c.Store_object = True
        twint.run.Lookup(c)
        users = twint.output.users_list
        user_dict = users[-1].__dict__
        print(user_dict)
        return [user_dict['bio'], user_dict['location']]
    except Exception as e:
        print(e)
        return ["", ""]
