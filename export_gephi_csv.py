from datetime import datetime
import csv

from get_invite_chain import edge_list, node_list

str_date = datetime.now().strftime("%Y%m%dT%H%M%S")

keys = edge_list[0].keys()
with open(str_date + '_edge_list.csv', 'w', newline='', encoding='utf-8')  as output_file:
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    dict_writer.writerows(edge_list)

keys = node_list[0].keys()
with open(str_date + '_node_list.csv', 'w', newline='', encoding='utf-8')  as output_file:
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    dict_writer.writerows(node_list)