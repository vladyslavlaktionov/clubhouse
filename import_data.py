import sqlite3

import numpy as np
import pandas as pd

# Read data from .db file
sql_db = sqlite3.connect('data/Clubhouse_Dataset_v1.db')
# Load data as pandas dataframe
df = pd.read_sql_query("SELECT * FROM user", sql_db)
