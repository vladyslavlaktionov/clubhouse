from IPython.core.display import HTML

from import_data import df
from notebook_setup import image_formatter

df_display = df.loc[df['name'].str.contains("Zuckerberg", case=False)]
HTML(df_display.to_html(formatters={'photo_url': image_formatter}, escape=False))