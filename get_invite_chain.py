import json

from get_twitter_bio import get_twitter
from import_data import df


def get_inviter(start_id):
    records = json.loads(df.loc[df['user_id'].isin([start_id])].to_json(orient='records'))
    for record in records:
        # print(record)
        bio, loc = get_twitter(record['twitter'])

        node = {"Id": record['user_id'],
                "Size": record['num_followers'],
                "name": record['name'],
                "photo_url": record['photo_url'],
                "twitter": record['twitter'],
                "instagram": record['instagram'],
                "num_following": record['num_following'],
                "time_created": record['time_created'],
                "tw_bio": bio,
                "tw_loc": loc,
                "Image": str(record['user_id']) + ".png"
                }
        if record['invited_by_user_profile'] != "null":
            edge = {"Source": record['invited_by_user_profile'],
                    "Target": record['user_id']}
        else:
            edge = False
    return node, edge


def get_invitee(start_id):
    nodes = []
    edges = []
    records = json.loads(df.loc[df['invited_by_user_profile'].isin([start_id])].to_json(orient='records'))
    for record in records:
        bio, loc = get_twitter(record['twitter'])

        node = {"Id": record['user_id'],
                "Size": record['num_followers'],
                "name": record['name'],
                "photo_url": record['photo_url'],
                "twitter": record['twitter'],
                "instagram": record['instagram'],
                "num_following": record['num_following'],
                "time_created": record['time_created'],
                "tw_bio": bio,
                "tw_loc": loc,
                "Image": str(record['user_id']) + ".png"
                }
        nodes.append(node)

        edge = {"Source": record['invited_by_user_profile'],
                "Target": record['user_id']}
        edges.append(edge)
    return nodes, edges


def get_upstream(start_id):
    nodes = []
    edges = []
    while True:
        # print(start_id)
        node, edge = get_inviter(start_id)
        nodes.append(node)
        if not edge:
            break
        else:
            edges.append(edge)
            start_id = edge['Source']
    return nodes, edges


def get_downstream(start_ids):
    all_nodes = []
    all_edges = []
    while True:
        next_ids = []
        for start_id in start_ids:
            # print(start_id)
            nodes, edges = get_invitee(start_id)
            all_nodes.extend(nodes)
            all_edges.extend(edges)
            next_ids.extend(list(set([i['Id'] for i in nodes])))

        if next_ids == []:
            break
        else:
            start_ids = next_ids

    return all_nodes, all_edges


start_id = 4  # input starting user_id
node_list, edge_list = get_upstream(start_id)
node_list2, edge_list2 = get_downstream([start_id])
node_list.extend(node_list2)
edge_list.extend(edge_list2)