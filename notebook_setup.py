from IPython.core.display import display, HTML
from PIL import Image
import requests
from io import BytesIO
import base64
import pandas as pd

# remove limit for column width
pd.set_option('display.max_colwidth', -1)


# convert your links to html tags
def path_to_image_html(path):
    return '<img src="' + path + '" width="60" >'


def get_thumbnail(path):
    response = requests.get(path)
    i = Image.open(BytesIO(response.content))
    i.thumbnail((100, 100), Image.LANCZOS)
    return i


def image_base64(im):
    if isinstance(im, str):
        im = get_thumbnail(im)
    with BytesIO() as buffer:
        im.save(buffer, 'jpeg')
        return base64.b64encode(buffer.getvalue()).decode()


def image_formatter(im):
    return f'<img src="data:image/jpeg;base64,{image_base64(im)}">'